---
title: konversation 1.7.5 released!
date: 2018-06-27
layout: post
---
Konversation v1.7.5 is a small bugfix release fixing the build with Qt 5.11. 

 Changes from Konversation 1.7.4 to 1.7.5: 
 
+ Fixed building against Qt 5.11. 
 

