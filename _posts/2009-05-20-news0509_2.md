---
title: Konversation 1.2-alpha1 has been released!
date: 2009-05-20
layout: post
---
We're happy to bring you this first public release of the KDE 4 version of Konversation.<br><br>

Despite the "alpha" moniker we've settled on for this one, mostly due to not yet being feature-complete (see below), this port has already been used productively by a fair number of people for some time and should be stable enough for general usage. In fact, certain features, notably DCC file transfers and auto-replace, are expected to be more robust than in Konversation 1.1.<br><br>

While this version largely achieves feature parity with Konversation 1.1 (and adds several new features on top), notable exceptions are the lack of support for marker lines as well as nick and channel context menus in the chat view. These are pending the merge of a rewritten chat view and will make a return before the final Konversation 1.2 release. Other known issues in this version include a lack of KDE 4 HIG compliance in the configuration dialog and various minor interface polish problems; these will be addressed as well.<br>

Enjoy, and don't forget to report any bugs you encounter!<br><br>

Changes from 1.1 to 1.2-alpha1:
 
+ Ported to KDE 4 (KDE 4.1 or higher is required). 
+ Enabled the (experimental, hackish) Amarok 2 support in the 'media' script. 
+ Fixed a bug that could cause channel notifications to be lost across reconnects. 
+ Removed the code to recreate hidden-to-tray state across application restarts. It was broken after the shutdown procedures were moved to a point in time after after the main window is hidden to cover quit-by-DCOP, and Konversation 1.1 features an explicit hidden startup option that fulfills user demands more accurately anyhow. This fixes a bug that made Konversation always hide to tray on startup regardless of the aforementioned option when the system tray icon was enabled. 
+ Added a network settings lookup fallback to retrieving the key of a channel. Previously, this relied solely on the channel's mode map. Closes the brief gap between a channel join and the server's reply to MODE where possible, so that e.g. reconnecting directly after auto-joining a channel with a key doesn't result in a failed rejoin due to not having the key by way of the MODE reply yet. 
+ Fixed opening URLs from the channel topic context menu in Channel List tabs. 
+ When connecting to multiple selected unexpanded network items from the Server List, don't also try to connect to the hidden server sub-items selected by implication, avoiding unwanted connection duplicates. 
+ Mask the password field in the Quick Connect dialog. 
+ Fixed a bug causing passive DCC file transfers to stall at 99%. 
+ Fixed "/leave" command in queries. 
+ Fixed auto-replace rules containing commas in the pattern not being loaded correctly from the config file. 
+ Fixed non-regex mode auto-replace rules containing regex special characters and character sequences not working correctly. 
+ Improved performance of non-regex mode auto-replace rules. 
+ Added option to open log files with the system text editor instead of the built-in log viewer. 
+ Made the Oxygen nicklist icon theme the default nicklist icon theme. 
+ Removed the bundled 'weather' script (it relied on a KDE 3 service no longer present in KDE 4; a replacement will need to adopt a new approach). 
+ Fix sending and receiving of files with names containing spaces 
+ DCC Protocol adjustment to proper handle passive DCC resume/accept requests. (this breaks passive-resume compatibly with &lt;konversation-1.2) 
+ Send proper DCC reject commands when rejecting a queued receive. 
+ Improved error recovery during dcc send. 
+ Fix time left for transfers that finished in under 1 sec displaying infinite time left. 
+ Increased default DCC buffer size to 16384 to reduce CPU load while sending or receiving files. 
+ Added KNotify events for "Highlight triggered", "DCC transfer complete" and "DCC transfer error". 
+ Fixed Automatic User Information Look Up not being started upon channel join on some IRC servers (namely those that don't send RPL_CHANNELCREATED after joining a channel, such at those used by IRCnet). 
+ Updated the server hostname for the pre-configured Freenode network to the one given on their website these days, 'chat.freenode.net'. 
+ Added support for browsing the input line history by using the mouse wheel. 
+ Fixed problems the bundled 'tinyurl' script had with certain URLs by converting it to use the TinyURL API rather than screen scraping. 
+ Added initial support for the MODES parameter of RPL_ISUPPORT. When giving or taking op, half-op or voice to/from multiple people at once, Konversation will now combine as many of them into a single MODE command as the server advertises it supports (as long as it advertises an actual value; the value-less unlimited MODES case is not supported yet as it requires more work on limiting MODE commands to the 512 byte IRC message buffer limit for extreme cases). If MODES is not given at all by the server, the fallback is an RFC1459-compliant value of 3. 
+ Added support for formatting variable expansion in the replacement part of auto-replace rules. 
+ Rewrote multi-line paste editor, improving handling and appearance. 
+ Added button to intelligently replace line breaks with spaces to the multi-line paste editor. 
 

